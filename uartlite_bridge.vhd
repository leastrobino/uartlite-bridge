--------------------------------------------------------------------------------
-- Title      : AXI UART Lite v2.0 PS to PCIe bridge
-- Project    :
--------------------------------------------------------------------------------
-- File       : uartlite_bridge.vhd
-- Author     : Lea Strobino <lea.strobino@cern.ch>
-- Company    : CERN - European Organization for Nuclear Research
-- Created    : 12/02/2021
-- Last update: 13/03/2021
-- Platform   :
-- Standard   : VHDL'93/02
--------------------------------------------------------------------------------
-- Description:
--------------------------------------------------------------------------------
-- Copyright (c) 2021 CERN
-- SPDX-License-Identifier: CERN-OHL-W-2.0
--------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author        Description
-- 13/03/2021  1.0      Lea Strobino  Created
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library UNISIM;
use UNISIM.vcomponents.all;

entity uartlite_bridge is
  generic (
    g_ps_reset_duration         : integer range 5 to 100 := 5;
    g_pcie_reset_duration       : integer range 5 to 100 := 5;
    g_led_enable                : integer range 0 to 1   := 1;
    g_led_pulse_duration        : integer range 2 to 1000000000 := 100000
  );
  port (
    -- AXI4-Lite slave interface from PS
    s_axi_ps_aclk               : in  std_logic;
    s_axi_ps_aresetn            : in  std_logic;
    s_axi_ps_awvalid            : in  std_logic;
    s_axi_ps_awready            : out std_logic;
    s_axi_ps_awaddr             : in  std_logic_vector(3 downto 0);
    s_axi_ps_wvalid             : in  std_logic;
    s_axi_ps_wready             : out std_logic;
    s_axi_ps_wdata              : in  std_logic_vector(31 downto 0);
    s_axi_ps_wstrb              : in  std_logic_vector(3 downto 0);
    s_axi_ps_bvalid             : out std_logic;
    s_axi_ps_bready             : in  std_logic;
    s_axi_ps_bresp              : out std_logic_vector(1 downto 0);
    s_axi_ps_arvalid            : in  std_logic;
    s_axi_ps_arready            : out std_logic;
    s_axi_ps_araddr             : in  std_logic_vector(3 downto 0);
    s_axi_ps_rvalid             : out std_logic;
    s_axi_ps_rready             : in  std_logic;
    s_axi_ps_rdata              : out std_logic_vector(31 downto 0);
    s_axi_ps_rresp              : out std_logic_vector(1 downto 0);
    -- AXI4-Lite slave interface from PCIe
    s_axi_pcie_aclk             : in  std_logic;
    s_axi_pcie_aresetn          : in  std_logic;
    s_axi_pcie_awvalid          : in  std_logic;
    s_axi_pcie_awready          : out std_logic;
    s_axi_pcie_awaddr           : in  std_logic_vector(3 downto 0);
    s_axi_pcie_wvalid           : in  std_logic;
    s_axi_pcie_wready           : out std_logic;
    s_axi_pcie_wdata            : in  std_logic_vector(31 downto 0);
    s_axi_pcie_wstrb            : in  std_logic_vector(3 downto 0);
    s_axi_pcie_bvalid           : out std_logic;
    s_axi_pcie_bready           : in  std_logic;
    s_axi_pcie_bresp            : out std_logic_vector(1 downto 0);
    s_axi_pcie_arvalid          : in  std_logic;
    s_axi_pcie_arready          : out std_logic;
    s_axi_pcie_araddr           : in  std_logic_vector(3 downto 0);
    s_axi_pcie_rvalid           : out std_logic;
    s_axi_pcie_rready           : in  std_logic;
    s_axi_pcie_rdata            : out std_logic_vector(31 downto 0);
    s_axi_pcie_rresp            : out std_logic_vector(1 downto 0);
    -- Interrupts
    ps_irq_o                    : out std_logic;
    pcie_irq_req_o              : out std_logic;
    pcie_irq_ack_i              : in  std_logic;
    -- Activity LEDs
    led_rx_o                    : out std_logic := '0';
    led_tx_o                    : out std_logic := '0'
  );
end entity;

architecture RTL of uartlite_bridge is

  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s_axi_ps_aclk      : signal is "xilinx.com:signal:clock:1.0 s_axi_ps_aclk CLK";
  attribute X_INTERFACE_INFO of s_axi_ps_aresetn   : signal is "xilinx.com:signal:reset:1.0 s_axi_ps_aresetn RST";
  attribute X_INTERFACE_INFO of s_axi_ps_awvalid   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS AWVALID";
  attribute X_INTERFACE_INFO of s_axi_ps_awready   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS AWREADY";
  attribute X_INTERFACE_INFO of s_axi_ps_awaddr    : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS AWADDR";
  attribute X_INTERFACE_INFO of s_axi_ps_wvalid    : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS WVALID";
  attribute X_INTERFACE_INFO of s_axi_ps_wready    : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS WREADY";
  attribute X_INTERFACE_INFO of s_axi_ps_wdata     : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS WDATA";
  attribute X_INTERFACE_INFO of s_axi_ps_wstrb     : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS WSTRB";
  attribute X_INTERFACE_INFO of s_axi_ps_bvalid    : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS BVALID";
  attribute X_INTERFACE_INFO of s_axi_ps_bready    : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS BREADY";
  attribute X_INTERFACE_INFO of s_axi_ps_bresp     : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS BRESP";
  attribute X_INTERFACE_INFO of s_axi_ps_arvalid   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS ARVALID";
  attribute X_INTERFACE_INFO of s_axi_ps_arready   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS ARREADY";
  attribute X_INTERFACE_INFO of s_axi_ps_araddr    : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS ARADDR";
  attribute X_INTERFACE_INFO of s_axi_ps_rvalid    : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS RVALID";
  attribute X_INTERFACE_INFO of s_axi_ps_rready    : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS RREADY";
  attribute X_INTERFACE_INFO of s_axi_ps_rdata     : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS RDATA";
  attribute X_INTERFACE_INFO of s_axi_ps_rresp     : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PS RRESP";
  attribute X_INTERFACE_INFO of s_axi_pcie_aclk    : signal is "xilinx.com:signal:clock:1.0 s_axi_pcie_aclk CLK";
  attribute X_INTERFACE_INFO of s_axi_pcie_aresetn : signal is "xilinx.com:signal:reset:1.0 s_axi_pcie_aresetn RST";
  attribute X_INTERFACE_INFO of s_axi_pcie_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe AWVALID";
  attribute X_INTERFACE_INFO of s_axi_pcie_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe AWREADY";
  attribute X_INTERFACE_INFO of s_axi_pcie_awaddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe AWADDR";
  attribute X_INTERFACE_INFO of s_axi_pcie_wvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe WVALID";
  attribute X_INTERFACE_INFO of s_axi_pcie_wready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe WREADY";
  attribute X_INTERFACE_INFO of s_axi_pcie_wdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe WDATA";
  attribute X_INTERFACE_INFO of s_axi_pcie_wstrb   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe WSTRB";
  attribute X_INTERFACE_INFO of s_axi_pcie_bvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe BVALID";
  attribute X_INTERFACE_INFO of s_axi_pcie_bready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe BREADY";
  attribute X_INTERFACE_INFO of s_axi_pcie_bresp   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe BRESP";
  attribute X_INTERFACE_INFO of s_axi_pcie_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe ARVALID";
  attribute X_INTERFACE_INFO of s_axi_pcie_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe ARREADY";
  attribute X_INTERFACE_INFO of s_axi_pcie_araddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe ARADDR";
  attribute X_INTERFACE_INFO of s_axi_pcie_rvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe RVALID";
  attribute X_INTERFACE_INFO of s_axi_pcie_rready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe RREADY";
  attribute X_INTERFACE_INFO of s_axi_pcie_rdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe RDATA";
  attribute X_INTERFACE_INFO of s_axi_pcie_rresp   : signal is "xilinx.com:interface:aximm:1.0 S_AXI_PCIe RRESP";
  attribute X_INTERFACE_INFO of ps_irq_o           : signal is "xilinx.com:signal:interrupt:1.0 ps_irq_o INTERRUPT";

  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s_axi_ps_aclk      : signal is "ASSOCIATED_BUSIF S_AXI_PS";
  attribute X_INTERFACE_PARAMETER of s_axi_ps_aresetn   : signal is "POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_PARAMETER of s_axi_pcie_aclk    : signal is "ASSOCIATED_BUSIF S_AXI_PCIe";
  attribute X_INTERFACE_PARAMETER of s_axi_pcie_aresetn : signal is "POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_PARAMETER of ps_irq_o           : signal is "SENSITIVITY EDGE_RISING";

  signal ps_ctrl_rst_rx_fifo    : std_logic;
  signal ps_ctrl_rst_tx_fifo    : std_logic;
  signal ps_ctrl_enable_irq     : std_logic;
  signal ps_ctrl_wr             : std_logic;
  signal ps_ctrl_wr_d           : std_logic_vector(g_ps_reset_duration-1 downto 0);
  signal ps_rst_rx_fifo         : std_logic;
  signal ps_rst_tx_fifo         : std_logic;
  signal ps_irq_enabled         : std_logic;

  signal pcie_ctrl_rst_rx_fifo  : std_logic;
  signal pcie_ctrl_rst_tx_fifo  : std_logic;
  signal pcie_ctrl_enable_irq   : std_logic;
  signal pcie_ctrl_link_up      : std_logic;
  signal pcie_ctrl_wr           : std_logic;
  signal pcie_ctrl_wr_d         : std_logic_vector(g_pcie_reset_duration-1 downto 0);
  signal pcie_rst_rx_fifo       : std_logic;
  signal pcie_rst_tx_fifo       : std_logic;
  signal pcie_irq_enabled       : std_logic;
  signal pcie_link_up           : std_logic;
  signal pcie_link_up_d         : std_logic_vector(1 downto 0);

  signal rx_fifo_di             : std_logic_vector(7 downto 0);
  signal rx_fifo_do             : std_logic_vector(7 downto 0);
  signal rx_fifo_wr             : std_logic;
  signal rx_fifo_wren           : std_logic;
  signal rx_fifo_rd             : std_logic;
  signal rx_fifo_rden           : std_logic;
  signal rx_fifo_empty          : std_logic;
  signal rx_fifo_empty_d        : std_logic_vector(1 downto 0);
  signal rx_fifo_full           : std_logic;
  signal rx_fifo_full_d         : std_logic_vector(1 downto 0);
  signal rx_fifo_valid          : std_logic;
  signal rx_fifo_rst            : std_logic;

  signal tx_fifo_di             : std_logic_vector(7 downto 0);
  signal tx_fifo_do             : std_logic_vector(7 downto 0);
  signal tx_fifo_wr             : std_logic;
  signal tx_fifo_wren           : std_logic;
  signal tx_fifo_rd             : std_logic;
  signal tx_fifo_rden           : std_logic;
  signal tx_fifo_empty          : std_logic;
  signal tx_fifo_empty_d        : std_logic_vector(1 downto 0);
  signal tx_fifo_full           : std_logic;
  signal tx_fifo_full_d         : std_logic_vector(1 downto 0);
  signal tx_fifo_full_link_up   : std_logic;
  signal tx_fifo_valid          : std_logic;
  signal tx_fifo_rst            : std_logic;

  signal reserved_0, reserved_1 : std_logic_vector(63 downto 8);

  component uartlite_axi4lite is
    port (
      aclk                      : in  std_logic;
      areset_n                  : in  std_logic;
      awvalid                   : in  std_logic;
      awready                   : out std_logic;
      awaddr                    : in  std_logic_vector(3 downto 2);
      awprot                    : in  std_logic_vector(2 downto 0) := "000";
      wvalid                    : in  std_logic;
      wready                    : out std_logic;
      wdata                     : in  std_logic_vector(31 downto 0);
      wstrb                     : in  std_logic_vector(3 downto 0);
      bvalid                    : out std_logic;
      bready                    : in  std_logic;
      bresp                     : out std_logic_vector(1 downto 0);
      arvalid                   : in  std_logic;
      arready                   : out std_logic;
      araddr                    : in  std_logic_vector(3 downto 2);
      arprot                    : in  std_logic_vector(2 downto 0) := "000";
      rvalid                    : out std_logic;
      rready                    : in  std_logic;
      rdata                     : out std_logic_vector(31 downto 0);
      rresp                     : out std_logic_vector(1 downto 0);
      rx_fifo_i                 : in  std_logic_vector(7 downto 0);
      rx_fifo_rd_o              : out std_logic;
      tx_fifo_o                 : out std_logic_vector(7 downto 0);
      tx_fifo_wr_o              : out std_logic;
      stat_rx_fifo_valid_i      : in  std_logic;
      stat_rx_fifo_full_i       : in  std_logic;
      stat_tx_fifo_empty_i      : in  std_logic;
      stat_tx_fifo_full_i       : in  std_logic;
      stat_irq_enabled_i        : in  std_logic;
      stat_overrun_error_i      : in  std_logic := '0';
      stat_frame_error_i        : in  std_logic := '0';
      stat_parity_error_i       : in  std_logic := '0';
      ctrl_rst_rx_fifo_o        : out std_logic;
      ctrl_rst_tx_fifo_o        : out std_logic;
      ctrl_enable_irq_o         : out std_logic;
      ctrl_link_up_o            : out std_logic;
      ctrl_wr_o                 : out std_logic
    );
  end component;

  component activity_led is
    generic (
      g_pulse_duration          : integer range 2 to 1000000000
    );
    port (
      clk_i                     : in  std_logic;
      reset_n_i                 : in  std_logic;
      signal_i                  : in  std_logic_vector;
      led_o                     : out std_logic_vector
    );
  end component;

begin

  -- PS UART Lite
  cmp_uartlite_ps : component uartlite_axi4lite
  port map (
    aclk                        => s_axi_ps_aclk,
    areset_n                    => s_axi_ps_aresetn,
    awvalid                     => s_axi_ps_awvalid,
    awready                     => s_axi_ps_awready,
    awaddr                      => s_axi_ps_awaddr(3 downto 2),
    wvalid                      => s_axi_ps_wvalid,
    wready                      => s_axi_ps_wready,
    wdata                       => s_axi_ps_wdata,
    wstrb                       => s_axi_ps_wstrb,
    bvalid                      => s_axi_ps_bvalid,
    bready                      => s_axi_ps_bready,
    bresp                       => s_axi_ps_bresp,
    arvalid                     => s_axi_ps_arvalid,
    arready                     => s_axi_ps_arready,
    araddr                      => s_axi_ps_araddr(3 downto 2),
    rvalid                      => s_axi_ps_rvalid,
    rready                      => s_axi_ps_rready,
    rdata                       => s_axi_ps_rdata,
    rresp                       => s_axi_ps_rresp,
    rx_fifo_i                   => rx_fifo_do,
    rx_fifo_rd_o                => rx_fifo_rd,
    tx_fifo_o                   => tx_fifo_di,
    tx_fifo_wr_o                => tx_fifo_wr,
    stat_rx_fifo_valid_i        => rx_fifo_valid,
    stat_rx_fifo_full_i         => rx_fifo_full_d(1),
    stat_tx_fifo_empty_i        => tx_fifo_empty_d(1),
    stat_tx_fifo_full_i         => tx_fifo_full_link_up,
    stat_irq_enabled_i          => ps_irq_enabled,
    ctrl_rst_rx_fifo_o          => ps_ctrl_rst_rx_fifo,
    ctrl_rst_tx_fifo_o          => ps_ctrl_rst_tx_fifo,
    ctrl_enable_irq_o           => ps_ctrl_enable_irq,
    ctrl_wr_o                   => ps_ctrl_wr
  );
  rx_fifo_valid                 <= not rx_fifo_empty;
  tx_fifo_full_link_up          <= tx_fifo_full and pcie_link_up_d(1);

  -- PCIe UART Lite
  cmp_uartlite_pcie : component uartlite_axi4lite
  port map (
    aclk                        => s_axi_pcie_aclk,
    areset_n                    => s_axi_pcie_aresetn,
    awvalid                     => s_axi_pcie_awvalid,
    awready                     => s_axi_pcie_awready,
    awaddr                      => s_axi_pcie_awaddr(3 downto 2),
    wvalid                      => s_axi_pcie_wvalid,
    wready                      => s_axi_pcie_wready,
    wdata                       => s_axi_pcie_wdata,
    wstrb                       => s_axi_pcie_wstrb,
    bvalid                      => s_axi_pcie_bvalid,
    bready                      => s_axi_pcie_bready,
    bresp                       => s_axi_pcie_bresp,
    arvalid                     => s_axi_pcie_arvalid,
    arready                     => s_axi_pcie_arready,
    araddr                      => s_axi_pcie_araddr(3 downto 2),
    rvalid                      => s_axi_pcie_rvalid,
    rready                      => s_axi_pcie_rready,
    rdata                       => s_axi_pcie_rdata,
    rresp                       => s_axi_pcie_rresp,
    rx_fifo_i                   => tx_fifo_do,
    rx_fifo_rd_o                => tx_fifo_rd,
    tx_fifo_o                   => rx_fifo_di,
    tx_fifo_wr_o                => rx_fifo_wr,
    stat_rx_fifo_valid_i        => tx_fifo_valid,
    stat_rx_fifo_full_i         => tx_fifo_full_d(1),
    stat_tx_fifo_empty_i        => rx_fifo_empty_d(1),
    stat_tx_fifo_full_i         => rx_fifo_full,
    stat_irq_enabled_i          => pcie_irq_enabled,
    ctrl_rst_rx_fifo_o          => pcie_ctrl_rst_tx_fifo,
    ctrl_rst_tx_fifo_o          => pcie_ctrl_rst_rx_fifo,
    ctrl_enable_irq_o           => pcie_ctrl_enable_irq,
    ctrl_link_up_o              => pcie_ctrl_link_up,
    ctrl_wr_o                   => pcie_ctrl_wr
  );
  tx_fifo_valid                 <= not tx_fifo_empty;

  -- Activity LEDs
  gen_activity_led : if g_led_enable = 1 generate
    cmp_activity_led : component activity_led
    generic map (
      g_pulse_duration          => g_led_pulse_duration
    )
    port map (
      clk_i                     => s_axi_ps_aclk,
      reset_n_i                 => s_axi_ps_aresetn,
      signal_i(0)               => rx_fifo_rd,
      signal_i(1)               => tx_fifo_wr,
      led_o(0)                  => led_rx_o,
      led_o(1)                  => led_tx_o
    );
  end generate;

  -- PS UART Lite control register
  process (s_axi_ps_aclk) begin
    if rising_edge(s_axi_ps_aclk) then
      if s_axi_ps_aresetn = '0' then
        ps_rst_rx_fifo <= '0';
        ps_rst_tx_fifo <= '0';
        ps_irq_enabled <= '0';
        ps_ctrl_wr_d   <= (others => '0');
      else
        if ps_ctrl_wr = '1' and ps_ctrl_rst_rx_fifo = '1' then
          ps_rst_rx_fifo <= '1';
        elsif ps_ctrl_wr_d(ps_ctrl_wr_d'left) = '1' then
          ps_rst_rx_fifo <= '0';
        end if;
        if ps_ctrl_wr = '1' and ps_ctrl_rst_tx_fifo = '1' then
          ps_rst_tx_fifo <= '1';
        elsif ps_ctrl_wr_d(ps_ctrl_wr_d'left) = '1' then
          ps_rst_tx_fifo <= '0';
        end if;
        if ps_ctrl_wr = '1' then
          ps_irq_enabled <= ps_ctrl_enable_irq;
        end if;
        ps_ctrl_wr_d <= ps_ctrl_wr_d(ps_ctrl_wr_d'left-1 downto 0) & ps_ctrl_wr;
      end if;
    end if;
  end process;

  -- PCIe UART Lite control register
  process (s_axi_pcie_aclk) begin
    if rising_edge(s_axi_pcie_aclk) then
      if s_axi_pcie_aresetn = '0' then
        pcie_rst_rx_fifo <= '0';
        pcie_rst_tx_fifo <= '0';
        pcie_irq_enabled <= '0';
        pcie_link_up     <= '0';
        pcie_ctrl_wr_d   <= (others => '0');
      else
        if pcie_ctrl_wr = '1' and pcie_ctrl_rst_rx_fifo = '1' then
          pcie_rst_rx_fifo <= '1';
        elsif pcie_ctrl_wr_d(pcie_ctrl_wr_d'left) = '1' then
          pcie_rst_rx_fifo <= '0';
        end if;
        if pcie_ctrl_wr = '1' and pcie_ctrl_rst_tx_fifo = '1' then
          pcie_rst_tx_fifo <= '1';
        elsif pcie_ctrl_wr_d(pcie_ctrl_wr_d'left) = '1' then
          pcie_rst_tx_fifo <= '0';
        end if;
        if pcie_ctrl_wr = '1' then
          pcie_irq_enabled <= pcie_ctrl_enable_irq;
          pcie_link_up     <= pcie_ctrl_link_up;
        end if;
        pcie_ctrl_wr_d <= pcie_ctrl_wr_d(pcie_ctrl_wr_d'left-1 downto 0) & pcie_ctrl_wr;
      end if;
    end if;
  end process;

  -- RX FIFO (PCIe to PS)
  cmp_rx_fifo : component FIFO36E1
  generic map (
    DATA_WIDTH                  => 9,
    DO_REG                      => 1,
    EN_ECC_READ                 => false,
    EN_ECC_WRITE                => false,
    EN_SYN                      => false,
    FIFO_MODE                   => "FIFO36",
    FIRST_WORD_FALL_THROUGH     => true
  )
  port map (
    DI(63 downto 8)             => (others => '0'),
    DI(7 downto 0)              => rx_fifo_di,
    DO(63 downto 8)             => reserved_0,
    DO(7 downto 0)              => rx_fifo_do,
    WRCLK                       => s_axi_pcie_aclk,
    WREN                        => rx_fifo_wren,
    RDCLK                       => s_axi_ps_aclk,
    RDEN                        => rx_fifo_rden,
    EMPTY                       => rx_fifo_empty,
    FULL                        => rx_fifo_full,
    RST                         => rx_fifo_rst,
    DIP                         => (others => '0'),
    INJECTDBITERR               => '0',
    INJECTSBITERR               => '0',
    REGCE                       => '0',
    RSTREG                      => '0'
  );
  rx_fifo_wren                  <= rx_fifo_wr and not rx_fifo_full  and not rx_fifo_rst;
  rx_fifo_rden                  <= rx_fifo_rd and not rx_fifo_empty and not rx_fifo_rst;
  rx_fifo_rst                   <= not s_axi_ps_aresetn or not s_axi_pcie_aresetn or ps_rst_rx_fifo or pcie_rst_rx_fifo;

  -- TX FIFO (PS to PCIe)
  cmp_tx_fifo : component FIFO36E1
  generic map (
    DATA_WIDTH                  => 9,
    DO_REG                      => 1,
    EN_ECC_READ                 => false,
    EN_ECC_WRITE                => false,
    EN_SYN                      => false,
    FIFO_MODE                   => "FIFO36",
    FIRST_WORD_FALL_THROUGH     => true
  )
  port map (
    DI(63 downto 8)             => (others => '0'),
    DI(7 downto 0)              => tx_fifo_di,
    DO(63 downto 8)             => reserved_1,
    DO(7 downto 0)              => tx_fifo_do,
    WRCLK                       => s_axi_ps_aclk,
    WREN                        => tx_fifo_wren,
    RDCLK                       => s_axi_pcie_aclk,
    RDEN                        => tx_fifo_rden,
    EMPTY                       => tx_fifo_empty,
    FULL                        => tx_fifo_full,
    RST                         => tx_fifo_rst,
    DIP                         => (others => '0'),
    INJECTDBITERR               => '0',
    INJECTSBITERR               => '0',
    REGCE                       => '0',
    RSTREG                      => '0'
  );
  tx_fifo_wren                  <= tx_fifo_wr and not tx_fifo_full  and not tx_fifo_rst;
  tx_fifo_rden                  <= tx_fifo_rd and not tx_fifo_empty and not tx_fifo_rst;
  tx_fifo_rst                   <= not s_axi_ps_aresetn or not s_axi_pcie_aresetn or ps_rst_tx_fifo or pcie_rst_tx_fifo;

  -- Clock domain crossing
  --   rx_fifo_empty and tx_fifo_full  are synchronous to s_axi_ps_aclk   (RX FIFO RDCLK and TX FIFO WRCLK),
  --   rx_fifo_full  and tx_fifo_empty are synchronous to s_axi_pcie_aclk (RX FIFO WRCLK and TX FIFO RDCLK),
  --   pcie_link_up is synchronous to s_axi_pcie_aclk (PCIe UART Lite control register).
  process (s_axi_ps_aclk, s_axi_pcie_aclk) begin
    if rising_edge(s_axi_ps_aclk) then
      rx_fifo_full_d  <= rx_fifo_full_d(0)  & rx_fifo_full;
      tx_fifo_empty_d <= tx_fifo_empty_d(0) & tx_fifo_empty;
      pcie_link_up_d  <= pcie_link_up_d(0)  & pcie_link_up;
    end if;
    if rising_edge(s_axi_pcie_aclk) then
      rx_fifo_empty_d <= rx_fifo_empty_d(0) & rx_fifo_empty;
      tx_fifo_full_d  <= tx_fifo_full_d(0)  & tx_fifo_full;
    end if;
  end process;

  -- PS interrupt
  process (s_axi_ps_aclk)
    variable v_rx_fifo_empty, v_tx_fifo_empty : std_logic;
  begin
    if rising_edge(s_axi_ps_aclk) then
      if s_axi_ps_aresetn = '0' then
        v_rx_fifo_empty := '1';
        v_tx_fifo_empty := '0';
        ps_irq_o <= '0';
      else
        ps_irq_o <= '0';
        if ps_irq_enabled = '1' then
          if v_rx_fifo_empty = '1' and rx_fifo_empty = '0' then  -- falling edge
            ps_irq_o <= '1';
          end if;
          if v_tx_fifo_empty = '0' and tx_fifo_empty_d(1) = '1' then  -- rising edge
            ps_irq_o <= '1';
          end if;
        end if;
        v_rx_fifo_empty := rx_fifo_empty;
        v_tx_fifo_empty := tx_fifo_empty_d(1);
      end if;
    end if;
  end process;

  -- PCIe interrupt
  process (s_axi_pcie_aclk)
    variable v_rx_fifo_empty, v_tx_fifo_empty : std_logic;
  begin
    if rising_edge(s_axi_pcie_aclk) then
      if s_axi_pcie_aresetn = '0' then
        v_rx_fifo_empty := '0';
        v_tx_fifo_empty := '1';
        pcie_irq_req_o  <= '0';
      else
        if pcie_irq_ack_i = '1' then
          pcie_irq_req_o <= '0';
        elsif pcie_irq_enabled = '1' then
          if v_rx_fifo_empty = '0' and rx_fifo_empty_d(1) = '1' then  -- rising edge
            pcie_irq_req_o <= '1';
          end if;
          if v_tx_fifo_empty = '1' and tx_fifo_empty = '0' then  -- falling edge
            pcie_irq_req_o <= '1';
          end if;
        end if;
        v_rx_fifo_empty := rx_fifo_empty_d(1);
        v_tx_fifo_empty := tx_fifo_empty;
      end if;
    end if;
  end process;

end architecture;
