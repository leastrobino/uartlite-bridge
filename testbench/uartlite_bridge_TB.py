# ------------------------------------------------------------------------------
#  Title      : AXI UART Lite v2.0 PS to PCIe bridge testbench
#  Project    :
# ------------------------------------------------------------------------------
#  File       : uartlite_bridge_TB.py
#  Author     : Lea Strobino <lea.strobino@cern.ch>
#  Company    : CERN - European Organization for Nuclear Research
#  Created    : 14/02/2021
#  Last update:
#  Platform   : cocotb 1.4
#  Standard   : Python 3
# ------------------------------------------------------------------------------
#  Description:
# ------------------------------------------------------------------------------
#  Copyright (c) 2021 CERN
#  SPDX-License-Identifier: CERN-OHL-W-2.0
# ------------------------------------------------------------------------------
#  Revisions  :
#  Date        Version  Author        Description
#  14/02/2021  1.0      Lea Strobino  Created
# ------------------------------------------------------------------------------

import cocotb
from cocotb.binary import *
from cocotb.clock import *
from cocotb.result import *
from cocotb.triggers import *
from cocotb.drivers.amba import AXI4LiteMaster

class uartlite_bridge_TB(object):

    def __init__(self, dut):

        self.dut = dut

        self.clk_ps = dut.s_axi_ps_aclk
        cocotb.fork(Clock(self.clk_ps, 10, 'ns').start())

        self.clk_pcie = dut.s_axi_pcie_aclk
        cocotb.fork(Clock(self.clk_pcie, 8, 'ns').start())

        self.axi_ps = AXI4LiteMaster(dut, 's_axi_ps', self.clk_ps)
        self.axi_pcie = AXI4LiteMaster(dut, 's_axi_pcie', self.clk_pcie)

        dut.pcie_irq_ack_i.setimmediatevalue(0)

    async def reset(self):

        # Reset for 5 clock cycles
        self.dut.s_axi_ps_aresetn.setimmediatevalue(0)
        self.dut.s_axi_pcie_aresetn.setimmediatevalue(0)
        await Timer(1)
        await ClockCycles(self.clk_ps, 5)

        # Run for 5 clock cycles
        self.dut.s_axi_ps_aresetn <= 1
        self.dut.s_axi_pcie_aresetn <= 1
        await ClockCycles(self.clk_ps, 5)

    async def ps_write(self, data):
        tx_fifo_full = 1
        while tx_fifo_full:
            stat = await self.axi_ps.read(0x08)
            tx_fifo_full = (stat >> 3) & 1
        await self.axi_ps.write(0x04, data)

    async def ps_read(self):
        rx_fifo_valid = 0
        while not rx_fifo_valid:
            stat = await self.axi_ps.read(0x08)
            rx_fifo_valid = stat & 1
        data = await self.axi_ps.read(0x00)
        return int(data)

    async def ps_reset(self):
        await self.axi_ps.write(0x0C, 0b11)

    async def ps_enable_irq(self, enable_irq=1):
        await self.axi_ps.write(0x0C, bool(enable_irq) << 4)

    async def pcie_write(self, data):
        tx_fifo_full = 1
        while tx_fifo_full:
            stat = await self.axi_pcie.read(0x08)
            tx_fifo_full = (stat >> 3) & 1
        await self.axi_pcie.write(0x04, data)

    async def pcie_read(self):
        rx_fifo_valid = 0
        while not rx_fifo_valid:
            stat = await self.axi_pcie.read(0x08)
            rx_fifo_valid = stat & 1
        data = await self.axi_pcie.read(0x00)
        return int(data)

    async def pcie_reset(self):
        await self.axi_pcie.write(0x0C, 0b11)

    async def pcie_enable_irq(self, enable_irq=1):
        await self.axi_pcie.write(0x0C, bool(enable_irq) << 4)

    async def pcie_link_up(self, link_up=1):
        await self.axi_pcie.write(0x0C, bool(link_up) << 8)

@cocotb.test()
async def T00_PS_to_PCIe(dut):

    TB = uartlite_bridge_TB(dut)
    await TB.reset()

    for i in range(16, 32):
        await TB.ps_write(i)

    await ClockCycles(TB.clk_ps, 10)

    for i in range(16, 24):
        data = await TB.pcie_read()
        if data != i:
            msg = 'Read failed (expected value: 0x{:02X}, actual value: 0x{:02X})'.format(i, data)
            raise TestFailure(msg)

    await ClockCycles(TB.clk_ps, 10)

    await TB.ps_reset()

    await ClockCycles(TB.clk_ps, 16)

@cocotb.test()
async def T01_PCIe_to_PS(dut):

    TB = uartlite_bridge_TB(dut)
    await TB.reset()

    for i in range(16, 32):
        await TB.pcie_write(i)

    await ClockCycles(TB.clk_pcie, 10)

    for i in range(16, 24):
        data = await TB.ps_read()
        if data != i:
            msg = 'Read failed (expected value: 0x{:02X}, actual value: 0x{:02X})'.format(i, data)
            raise TestFailure(msg)

    await ClockCycles(TB.clk_pcie, 10)

    await TB.ps_reset()

    await ClockCycles(TB.clk_pcie, 43)

@cocotb.test()
async def T02_PS_to_PCIe_sim(dut):

    TB = uartlite_bridge_TB(dut)
    await TB.reset()

    await Combine(cocotb.fork(TB.ps_enable_irq()), cocotb.fork(TB.pcie_enable_irq()))

    async def T02_PS_to_PCIe_sim_write(TB):
        for i in range(128, 144):
            await TB.ps_write(i)

    async def T02_PS_to_PCIe_sim_read(TB):
        for i in range(128, 144):
            data = await TB.pcie_read()
            if data != i:
                msg = 'Read failed (expected value: 0x{:02X}, actual value: 0x{:02X})'.format(i, data)
                raise TestFailure(msg)

    async def T02_PS_to_PCIe_sim_irq_ack(TB):
        value = 0
        while True:
            await RisingEdge(TB.clk_pcie)
            TB.dut.pcie_irq_ack_i <= (TB.dut.pcie_irq_req_o.value and not value)
            value = TB.dut.pcie_irq_req_o.value

    write = cocotb.fork(T02_PS_to_PCIe_sim_write(TB))
    read = cocotb.fork(T02_PS_to_PCIe_sim_read(TB))
    irq_ack = cocotb.fork(T02_PS_to_PCIe_sim_irq_ack(TB))
    await Combine(write, read)

    await ClockCycles(TB.clk_ps, 69)
