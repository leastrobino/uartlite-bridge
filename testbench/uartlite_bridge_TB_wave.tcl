set dut ""

set signals { \
  {"AXI PS"   {s_axi_ps_aclk s_axi_ps_aresetn s_axi_ps_awaddr s_axi_ps_wdata s_axi_ps_wvalid s_axi_ps_bvalid s_axi_ps_araddr s_axi_ps_arvalid s_axi_ps_rdata s_axi_ps_rvalid}} \
  {"AXI PCIe" {s_axi_pcie_aclk s_axi_pcie_aresetn s_axi_pcie_awaddr s_axi_pcie_wdata s_axi_pcie_wvalid s_axi_pcie_bvalid s_axi_pcie_araddr s_axi_pcie_arvalid s_axi_pcie_rdata s_axi_pcie_rvalid}} \
  {"RX FIFO"  {rx_fifo_di rx_fifo_do rx_fifo_wren rx_fifo_rden rx_fifo_empty rx_fifo_full rx_fifo_rst}} \
  {"TX FIFO"  {tx_fifo_di tx_fifo_do tx_fifo_wren tx_fifo_rden tx_fifo_empty tx_fifo_full tx_fifo_rst}} \
  {"IRQ"      {ps_irq_enabled ps_irq_o pcie_irq_enabled pcie_irq_req_o pcie_irq_ack_i}} \
  {"LED"      {led_rx_o led_tx_o}} \
}

set radix_bin {}
set radix_dec {}
set radix_hex {}

foreach group $signals {
  gtkwave::/Edit/Insert_Comment [lindex $group 0]
  foreach item [lindex $group 1] {
    set signal $item
    if {[string first "." $item] == 0} {
      set item [string range $item 1 end]
    } elseif {$dut != ""} {
      set item $dut.$item
    }
    gtkwave::addSignalsFromList $item
    if {[lsearch -exact $radix_bin $signal] != -1} {
      gtkwave::/Edit/Data_Format/Binary
    } elseif {[lsearch -exact $radix_dec $signal] != -1} {
      gtkwave::/Edit/Data_Format/Decimal
    } elseif {[lsearch -exact $radix_hex $signal] != -1} {
      gtkwave::/Edit/Data_Format/Hex
    }
  }
}

gtkwave::/Edit/UnHighlight_All
gtkwave::/View/Left_Justified_Signals
gtkwave::/Time/Zoom/Zoom_Full
