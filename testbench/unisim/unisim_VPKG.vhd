library IEEE;
use IEEE.std_logic_1164.all;
library std;
use std.textio.all;

package vpkg is

  function slv_to_int(SLV : in std_logic_vector) return integer;

  function slv_to_str(SLV : in std_logic_vector) return string;

  function xil_pos_edge_inv (
    signal s : in std_ulogic;
    signal i : in std_ulogic
  ) return boolean;

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in string := "";
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in string := "";
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  );

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in integer;
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in integer;
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  );

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in boolean;
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in string := "";
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  );

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in integer;
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in string := "";
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  );

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in real;
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in string := "";
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  );

end package;

package body vpkg is

  function slv_to_int(SLV: in std_logic_vector) return integer is
    variable int : integer;
  begin
    int := 0;
    for i in SLV'high downto SLV'low loop
      int := int * 2;
      if SLV(i) = '1' then
        int := int + 1;
      end if;
    end loop;
    return int;
  end slv_to_int;

  function slv_to_str(SLV : in std_logic_vector) return string is
    variable j : integer := SLV'length;
    variable str : string(SLV'length downto 1);
  begin
    for i in SLV'high downto SLV'low loop
      case SLV(i) is
        when '0' => str(j) := '0';
        when '1' => str(j) := '1';
        when 'X' => str(j) := 'X';
        when 'U' => str(j) := 'U';
        when others => str(j) := 'X';
      end case;
      j := j - 1;
    end loop;
    return str;
  end slv_to_str;

  function xil_pos_edge_inv (
    signal s : in std_ulogic;
    signal i : in std_ulogic
  ) return boolean is begin
    return (TO_X01(s) = not i) and (TO_X01(s'last_value) = i);
  end;

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in string := "";
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in string := "";
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  ) is
    variable Message : line;
  begin
    write(Message, HeaderMsg);
    write(Message, string'(" The attribute "));
    write(Message, GenericName);
    write(Message, string'(" on "));
    write(Message, EntityName);
    write(Message, string'(" instance "));
    write(Message, InstanceName);
    write(Message, string'(" is set to "));
    write(Message, GenericValue);
    write(Message, Unit);
    write(Message, '.' & LF);
    write(Message, ExpectedValueMsg);
    write(Message, ExpectedGenericValue);
    write(Message, Unit);
    write(Message, TailMsg);
    assert false report Message.all severity MsgSeverity;
    deallocate(Message);
  end GenericValueCheckMessage;

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in integer;
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in integer;
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  ) is
    variable Message : line;
  begin
    write(Message, HeaderMsg);
    write(Message, string'(" The attribute "));
    write(Message, GenericName);
    write(Message, string'(" on "));
    write(Message, EntityName);
    write(Message, string'(" instance "));
    write(Message, InstanceName);
    write(Message, string'(" is set to "));
    write(Message, GenericValue);
    write(Message, Unit);
    write(Message, '.' & LF);
    write(Message, ExpectedValueMsg);
    write(Message, ExpectedGenericValue);
    write(Message, Unit);
    write(Message, TailMsg);
    assert false report Message.all severity MsgSeverity;
    deallocate(Message);
  end GenericValueCheckMessage;

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in boolean;
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in string := "";
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  ) is
    variable Message : line;
  begin
    write(Message, HeaderMsg);
    write(Message, string'(" The attribute "));
    write(Message, GenericName);
    write(Message, string'(" on "));
    write(Message, EntityName);
    write(Message, string'(" instance "));
    write(Message, InstanceName);
    write(Message, string'(" is set to "));
    write(Message, GenericValue);
    write(Message, Unit);
    write(Message, '.' & LF);
    write(Message, ExpectedValueMsg);
    write(Message, ExpectedGenericValue);
    write(Message, Unit);
    write(Message, TailMsg);
    assert false report Message.all severity MsgSeverity;
    deallocate(Message);
  end GenericValueCheckMessage;

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in integer;
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in string := "";
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  ) is
    variable Message : line;
  begin
    write(Message, HeaderMsg);
    write(Message, string'(" The attribute "));
    write(Message, GenericName);
    write(Message, string'(" on "));
    write(Message, EntityName);
    write(Message, string'(" instance "));
    write(Message, InstanceName);
    write(Message, string'(" is set to "));
    write(Message, GenericValue);
    write(Message, Unit);
    write(Message, '.' & LF);
    write(Message, ExpectedValueMsg);
    write(Message, ExpectedGenericValue);
    write(Message, Unit);
    write(Message, TailMsg);
    assert false report Message.all severity MsgSeverity;
    deallocate(Message);
  end GenericValueCheckMessage;

  procedure GenericValueCheckMessage (
    constant HeaderMsg : in string := " Attribute Syntax Error ";
    constant GenericName : in string := "";
    constant EntityName : in string := "";
    constant InstanceName : in string := "";
    constant GenericValue : in real;
    constant Unit : in string := "";
    constant ExpectedValueMsg : in string := "";
    constant ExpectedGenericValue : in string := "";
    constant TailMsg : in string;
    constant MsgSeverity : in severity_level := WARNING
  ) is
    variable Message : line;
  begin
    write(Message, HeaderMsg);
    write(Message, string'(" The attribute "));
    write(Message, GenericName);
    write(Message, string'(" on "));
    write(Message, EntityName);
    write(Message, string'(" instance "));
    write(Message, InstanceName);
    write(Message, string'(" is set to "));
    write(Message, GenericValue);
    write(Message, Unit);
    write(Message, '.' & LF);
    write(Message, ExpectedValueMsg);
    write(Message, ExpectedGenericValue);
    write(Message, Unit);
    write(Message, TailMsg);
    assert false report Message.all severity MsgSeverity;
    deallocate(Message);
  end GenericValueCheckMessage;

end package body;
