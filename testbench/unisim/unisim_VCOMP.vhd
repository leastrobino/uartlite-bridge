library IEEE;
use IEEE.std_logic_1164.all;

package vcomponents is

  signal GSR : std_logic := '0';
  signal GTS : std_logic := '0';

  component FDRE
    generic (
      INIT : bit := '0';
      IS_C_INVERTED : bit := '0';
      IS_D_INVERTED : bit := '0';
      IS_R_INVERTED : bit := '0'
    );
    port (
      Q : out std_ulogic := TO_X01(INIT);
      C : in std_ulogic;
      CE : in std_ulogic;
      D : in std_ulogic;
      R : in std_ulogic
    );
  end component;

  component FDSE
    generic (
      INIT : bit := '1';
      IS_C_INVERTED : bit := '0';
      IS_D_INVERTED : bit := '0';
      IS_S_INVERTED : bit := '0'
    );
    port (
      Q : out std_ulogic := TO_X01(INIT);
      C : in std_ulogic;
      CE : in std_ulogic;
      D : in std_ulogic;
      S : in std_ulogic
    );
  end component;

  component FIFO36E1
    generic (
      ALMOST_EMPTY_OFFSET : bit_vector := X"0080";
      ALMOST_FULL_OFFSET : bit_vector := X"0080";
      DATA_WIDTH : integer := 4;
      DO_REG : integer := 1;
      EN_ECC_READ : boolean := FALSE;
      EN_ECC_WRITE : boolean := FALSE;
      EN_SYN : boolean := FALSE;
      FIFO_MODE : string := "FIFO36";
      FIRST_WORD_FALL_THROUGH : boolean := FALSE;
      INIT : bit_vector := X"000000000000000000";
      IS_RDCLK_INVERTED : bit := '0';
      IS_RDEN_INVERTED : bit := '0';
      IS_RSTREG_INVERTED : bit := '0';
      IS_RST_INVERTED : bit := '0';
      IS_WRCLK_INVERTED : bit := '0';
      IS_WREN_INVERTED : bit := '0';
      SIM_DEVICE : string := "7SERIES";
      SRVAL : bit_vector := X"000000000000000000"
    );
    port (
      ALMOSTEMPTY : out std_ulogic;
      ALMOSTFULL : out std_ulogic;
      DBITERR : out std_ulogic;
      DO : out std_logic_vector(63 downto 0);
      DOP : out std_logic_vector(7 downto 0);
      ECCPARITY : out std_logic_vector(7 downto 0);
      EMPTY : out std_ulogic;
      FULL : out std_ulogic;
      RDCOUNT : out std_logic_vector(12 downto 0);
      RDERR : out std_ulogic;
      SBITERR : out std_ulogic;
      WRCOUNT : out std_logic_vector(12 downto 0);
      WRERR : out std_ulogic;
      DI : in std_logic_vector(63 downto 0);
      DIP : in std_logic_vector(7 downto 0);
      INJECTDBITERR : in std_ulogic;
      INJECTSBITERR : in std_ulogic;
      RDCLK : in std_ulogic;
      RDEN : in std_ulogic;
      REGCE : in std_ulogic;
      RST : in std_ulogic;
      RSTREG : in std_ulogic;
      WRCLK : in std_ulogic;
      WREN : in std_ulogic
    );
  end component;

end package;
