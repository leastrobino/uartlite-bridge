--------------------------------------------------------------------------------
-- Title      :
-- Project    :
--------------------------------------------------------------------------------
-- File       : activity_led.vhd
-- Author     : Lea Strobino <lea.strobino@cern.ch>
-- Company    : CERN - European Organization for Nuclear Research
-- Created    : 13/03/2021
-- Last update:
-- Platform   :
-- Standard   : VHDL'93/02
--------------------------------------------------------------------------------
-- Description:
--------------------------------------------------------------------------------
-- Copyright (c) 2021 CERN
-- SPDX-License-Identifier: CERN-OHL-W-2.0
--------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author        Description
-- 13/03/2021  1.0      Lea Strobino  Created
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
library UNISIM;
use UNISIM.vcomponents.all;

entity activity_led is
  generic (
    g_pulse_duration : integer range 2 to 1000000000 := 100000
  );
  port (
    clk_i     : in  std_logic;
    reset_n_i : in  std_logic;
    signal_i  : in  std_logic_vector;
    led_o     : out std_logic_vector
  );
end entity;

architecture RTL of activity_led is

  constant c_COUNTER_WIDTH : natural := natural(ceil(log2(real(g_pulse_duration+1))));

  signal counter  : unsigned(c_COUNTER_WIDTH-1 downto 0);

  signal clk_en   : std_logic;
  signal reset    : std_logic;
  signal signal_d : std_logic_vector(signal_i'range);

begin

  gen_FD : for i in signal_i'reverse_range generate

    cmp_FDSE : component FDSE
    generic map (
      INIT => '0'
    )
    port map (
      D  => '0',
      Q  => signal_d(i),
      C  => clk_i,
      CE => clk_en,
      S  => signal_i(i)
    );

    cmp_FDRE : component FDRE
    generic map (
      INIT => '0'
    )
    port map (
      D  => signal_d(i),
      Q  => led_o(i),
      C  => clk_i,
      CE => clk_en,
      R  => reset
    );

  end generate;

  process (clk_i) begin
    if rising_edge(clk_i) then
      if reset_n_i = '0' then
        counter <= (others => '0');
        clk_en  <= '0';
      else
        counter <= counter+1;
        clk_en  <= '0';
        if counter >= g_pulse_duration-1 then
          counter <= (others => '0');
          clk_en  <= '1';
        end if;
      end if;
    end if;
  end process;

  reset <= not reset_n_i;

end architecture;
